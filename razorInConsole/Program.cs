﻿using Postal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace razorInConsole
{
    
    class Program
    {
        static AppDomain otherDomain;
        static void Main(string[] args)
        {
            otherDomain = AppDomain.CreateDomain("other domain");

            var otherType = typeof(OtherProgram);
            var obj = otherDomain.CreateInstanceAndUnwrap(
                                     otherType.Assembly.FullName,
                                     otherType.FullName) as OtherProgram;

            args = new[] { "hello", "world" };
            Console.WriteLine(AppDomain.CurrentDomain.FriendlyName);
            obj.Main(args);
            Console.ReadKey();
        }
    }

    public class OtherProgram : MarshalByRefObject
    {
        public void Main(string[] args)
        {
            var viewsPath = Path.GetFullPath(@"..\..\Views\Emails");

            var engines = new ViewEngineCollection();
            engines.Add(new FileSystemRazorViewEngine(viewsPath));
            EmailService service;
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
            smtpClient.EnableSsl = true;

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);

            //var viewsPath = Path.GetFullPath(@"Views");

            //var engines = new ViewEngineCollection();
            engines.Add(new FileSystemRazorViewEngine(viewsPath));

            service = new EmailService(engines, () => smtpClient);

            dynamic email = new Email("Tested");
            email.From = ConfigurationManager.AppSettings["FromEmail"];
            email.Subject = "labs";
            //email.Body = "<h1>Hello</h1>";
            email.IsBodyHtml = true;
            email.To = "mike.volinets@gmail.com";
            //var viewsPath = Path.GetFullPath(@"..\..\Views");
            //var engines = new ViewEngineCollection();
            //var engine = new FileSystemRazorViewEngine(viewsPath);

            // Argument type 'Postal.FileSystemRazorViewEngine' is
            // not assignable to parameter type 'System.Web.Mvc.IViewEngine'.
            //engines.Add(engine);

            //var service = new EmailService(engines);

            //dynamic email = new Email("Tested");

            try
            {
                service.SendAsync(email);
            }
            catch (Exception exp)
            {

            }
        }
    }
}
